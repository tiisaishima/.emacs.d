(define-package "helm-flyspell" "20160927.1648" "Helm extension for correcting words with flyspell" '((helm "1.6.5")) :url "https://github.com/pronobis/helm-flyspell" :keywords '("convenience"))
