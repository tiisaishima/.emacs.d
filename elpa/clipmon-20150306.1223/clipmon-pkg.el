(define-package "clipmon" "20150306.1223" "Clipboard monitor - watch system clipboard, add changes to kill ring/autoinsert" 'nil :url "https://github.com/bburns/clipmon" :keywords
  '("convenience"))
;; Local Variables:
;; no-byte-compile: t
;; End:
