(define-package "snakemake-mode" "1.3.0" "Major mode for editing Snakemake files"
  '((emacs "24")
    (cl-lib "0.5")
    (magit-popup "2.4.0"))
  :url "https://github.com/kyleam/snakemake-mode" :keywords
  '("tools"))
;; Local Variables:
;; no-byte-compile: t
;; End:
