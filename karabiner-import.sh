#!/bin/sh

cli=/Applications/Karabiner.app/Contents/Library/bin/karabiner

$cli set private.assign_command_m_to_down 1
/bin/echo -n .
$cli set private.assign_right_backet_to_control_right 1
/bin/echo -n .
$cli set remap.controlJ2enter 1
/bin/echo -n .
$cli set remap.jis_backquote_kana_eisuu 1
/bin/echo -n .
$cli set private.assign_return_to_right_backet 1
/bin/echo -n .
$cli set private.assign_control_i_to_tab 1
/bin/echo -n .
$cli set remap.jis_kana2commandR 1
/bin/echo -n .
$cli set option.emacsmode_controlM 1
/bin/echo -n .
$cli set private.assign_control_piriod_to_backspace 1
/bin/echo -n .
$cli set remap.jis_commandR2kana 1
/bin/echo -n .
$cli set remap.controlJ2return 1
/bin/echo -n .
$cli set remap.jis_eisuu2commandL 1
/bin/echo -n .
$cli set remap.jis_underscore2backslash 1
/bin/echo -n .
$cli set private.assign_command_to_arrows 1
/bin/echo -n .
$cli set remap.jis_commandL2eisuu 1
/bin/echo -n .
$cli set private.assign_command_k_to_right 1
/bin/echo -n .
/bin/echo
