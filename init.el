;;ローダパスの追加関数
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory
              (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-loadpath)
            (normal-top-level-add-subdirs-to-load-path))))))
;;shell command can be seen in minibuffer
(setq enable-recursive-minibuffers t)
;;make load paths
(add-to-load-path "/.els.d/")
;(add-to-load-path "/.els.d/auto-complete-1.0")
(add-to-load-path "/.els.d/ess/lisp")
(add-to-load-path "/.els.d/anything")
(add-to-load-path "/.els.d/tramp")
(add-to-load-path "/.els.d/yatex")

;;パッケージのセッティング
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
;; MELPA-stableを追加
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
;; Orgを追加
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
(global-hl-line-mode t)

;;; init-loaderのセッティング
(require 'init-loader)
(setq init-loader-show-log-after-init t)
;;; 設定ファイルのあるフォルダを指定
(setq inits_dir (expand-file-name "~/.emacs.d/inits"))
(init-loader-load "~/.emacs.d/inits")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(multi-term-default-dir "~/")
 '(package-selected-packages
   (quote
    (gist yaml-mode snakemake-mode groovy-mode magit ein google-translate auto-complete web-mode w3m visual-regexp-steroids seq s pcre2el multi-term markdown-mode lua-mode latex-preview-pane init-loader hiwin helm-migemo helm-flyspell haskell-mode foreign-regexp flycheck exec-path-from-shell clipmon bind-key ace-isearch)))
 '(tramp-verbose 8))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(web-mode-comment-face ((t (:foreground "#D9333F"))))
 '(web-mode-css-at-rule-face ((t (:foreground "#FF7F00"))))
 '(web-mode-css-pseudo-class-face ((t (:foreground "#FF7F00"))))
 '(web-mode-css-rule-face ((t (:foreground "#A0D8EF"))))
 '(web-mode-doctype-face ((t (:foreground "#82AE46"))))
 '(web-mode-html-attr-name-face ((t (:foreground "#C97586"))))
 '(web-mode-html-attr-value-face ((t (:foreground "#82AE46"))))
 '(web-mode-html-tag-face ((t (:foreground "#E6B422" :weight bold))))
 '(web-mode-server-comment-face ((t (:foreground "#D9333F")))))
