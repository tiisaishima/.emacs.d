(require 'multi-term)
(setq multi-term-program shell-file-name)
; emacs に認識させたいキーがある場合は、term-unbind-key-list に追加する
;;(add-to-list 'term-unbind-key-list "C-\\") ; IME の切り替えを有効とする
;; (add-to-list 'term-unbind-key-list "C-o")  ; IME の切り替えに C-o を設定している場合
(add-to-list 'term-unbind-key-list "C-o") ; Anything
(add-to-list 'term-unbind-key-list "C-t") ; widow split
(add-to-list 'term-unbind-key-list "M-t") ; widow split
(add-to-list 'term-unbind-key-list "C-b") ; widow kill
(add-to-list 'term-unbind-key-list "C-a") ; kill buffer
(add-to-list 'term-unbind-key-list "C-f") ; open file
(add-to-list 'term-unbind-key-list "C-l") ; copy region
(add-to-list 'term-unbind-key-list "C-k") ; cut region
(add-to-list 'term-unbind-key-list "C-u") ; iteration
(add-to-list 'term-unbind-key-list "C-;") ; zoom in
(add-to-list 'term-unbind-key-list "C-:") ; zoom out
(add-to-list 'term-unbind-key-list "C-v") ; git update
;; terminal に直接通したいキーがある場合は、以下をアンコメントする
;(delete "<ESC>" term-unbind-key-list)
;;(delete "\C-l" term-unbind-key-list)
;; (delete "C-h" term-unbind-key-list)
;; (delete "C-z" term-unbind-key-list)
;;(delete "\C-x" term-unbind-key-list)
;;(delete "\C-c" term-unbind-key-list)
;;(delete "\C-y" term-unbind-key-list)

(defun bn-check (bn)
  (setq bn (car (split-string bn "*")))
  (if (not (not (get-buffer bn)))
      (setq bn (bn-check (concat bn "*"))))
    bn)
(defun buff-cd ()
  (interactive)
  (setq bn (buffer-name))
  (setq pos default-directory)
  (if (stringp pos)
      (setq pos (car (last (split-string pos "/") 2)))
    (setq pos nil))
  (if (stringp pos)
      (setq bn (concat "<term>@" pos)))
  (rename-buffer (bn-check bn)))
;; shell の割り込みを機能させる
(defadvice term-interrupt-subjob (around ad-term-interrupt-subjob activate)
  (term-send-raw-string (kbd "C-c")))
;; multi-term
(add-hook 'term-mode-hook
	  '(lambda ()
	     ;;rename buffer buffer@current directory
	     ;;anything-sourcesにshell履歴を追加
	     ;(setq anything-tmp (cons (car anything-sources) (cons 'anything-c-source-shell-history (cdr anything-sources))))
	     ;(set (make-local-variable 'anything-sources) nil)
	     ;(set (make-local-variable 'anything-sources) anything-tmp)
	     ;;ESC keyがそのままきく
	     ;(define-key term-raw-map (kbd "ESC") 'term-send-raw)
	     ;;コマンドを検索
	     (define-key term-raw-map "\C-r" 'term-send-raw)
	     (define-key term-raw-map "\C-s" 'term-send-raw)
	     ;; C-. を term 内文字削除にする
	     (define-key term-raw-map (kbd "C-.") 'term-send-backspace)
	     ;; C-y を term 内ペーストにする
	     (define-key term-raw-map (kbd "C-y") 'term-paste)
	     ;; C-y を term 内ペーストにする
	     (define-key term-raw-map (kbd "M-n") 'term-send-down)	     
	     ;; C-y を term 内ペーストにする
	     (define-key term-raw-map (kbd "M-p") 'term-send-up)	     
	     ;;cursor transitoion
	     (define-key term-raw-map "\M-k" 'term-send-right)
	     (define-key term-raw-map "\M-j" 'term-send-left)	
	     ;; M-h をforward word
	     (define-key term-raw-map (kbd "M-l") 'term-send-forward-word)
	     ;; M-h をbackward word
	     (define-key term-raw-map (kbd "M-h") 'term-send-backward-word)
	     (define-key term-raw-map (kbd "\M-y") 'term-send-move-beginning-of-line)
	     (define-key term-raw-map [?\M-/] 'term-send-move-end-of-line)
	     (define-key term-raw-map (kbd "C-SPC") 'term-send-set-mark-command)
	     ;; M-h をforward word
	     ;(define-key term-raw-map (kbd "M-k") 'term-send-nex)
	     ;; M-h をbackward word
	     ;(define-key term-raw-map (kbd "M-j") 'term-send-backward)
	     ;; esc
	     ;(define-key term-raw-map (kbd "ESC ESC") 'term-send-raw)
	     (define-key term-raw-map (kbd "C-k") 'term-send-forward-kill-word)
	     (define-key term-raw-map (kbd "M-<backspace>") 'term-send-backward-kill-word)
	     (define-key term-raw-map (kbd "M-DEL") 'term-send-backward-kill-word)
	     (define-key term-raw-map (kbd "C-v") nil)
	     ;; C-. を term 内文字削除にする
	     (define-key term-raw-map (kbd "M-.") (kbd "C-u 10 M-m"))
	     ;; C-. を term 内文字削除にする
	     (define-key term-raw-map (kbd "M-o") (kbd "C-u 10 M-i"))
	     (define-key term-raw-map (kbd "M-v") 'toggle-term-view))
	     (define-key term-raw-map (kbd "M-e") 'buff-cd)
	     ;(buff-cd)
	  
	  )
(defun toggle-term-view () (interactive)
  (cond ((eq major-mode 'term-mode)
     (fundamental-mode)
     (view-mode)
     (local-set-key (kbd "M-v") 'toggle-term-view)
     (setq multi-term-cursor-point (point)))
    ((eq major-mode 'fundamental-mode)
     (goto-char multi-term-cursor-point)
     (multi-term-internal))))
;; C-c m で multi-term を起動する
(global-set-key (kbd "C-c m") 'multi-term)
