;;aspell
(setq ispell-program-name "aspell")
;;setting font
(add-to-list 'default-frame-alist
             '(font . "-*-Menlo-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1"))
 
;; set input method
;(setq default-input-method "japanese-skk")
(set-default-coding-systems 'utf-8) 
;; ddskk jisyo
(setq skk-user-directory "~/Library/Application Support/AquaSKK/")
					;setting load-path
(setq user-emacs-directory "~/.emacs.d/")
(setq font-size 12)
