;;execute git update
(defun git-update ()
  (interactive)
  (setq comment (read-string "Write comment>")) ; read comment from minibuffer
  (setq comment (concat (buffer-name) " : " comment))
  (setq gub (get-buffer-create "git_update_output")) ; buffer of git update
  (call-process "~/usr/bin/git_update" nil gub nil (eval comment)) ; execute git_update
  (setq pf (read-string "Push this commit? (y or no)")) ; read comment from minibuffer
  (if (equal pf "y")
        (call-process "git" nil gub nil "push")) ; execute git_upda
  (setq nh (* (/ (window-total-height) 3) 2)) ; new window height
  (setq stw (split-window-below nh)) ; stdout window
  (set-window-buffer stw gub) ; display result of git_update
  (read-string "Please return") ; wait enter
  (delete-window stw)
  (kill-buffer gub))
