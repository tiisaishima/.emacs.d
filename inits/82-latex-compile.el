(require 'tex-mode)
(defun save-compile-latex-base (compiler)
  (save-buffer)
  (setq lvb (get-buffer-create (concat "latex_viewer_output:" (buffer-file-name)))) ; buffer of latex viewer
  (when (not (eq 
       (call-process compiler  nil lvb nil (buffer-file-name));; compilej
       0))
      (setq nh (* (/ (window-total-height) 2) 1)) ; new window height
       (setq stw (split-window-below nh)) ; stdout window
       (set-window-buffer stw lvb) ; display result of git_update
       (read-string "Please return") ; wait enter
       (delete-window stw)
       (kill-buffer lvb))
  (setq pdf-file-name (replace-regexp-in-string "tex" "pdf" (buffer-file-name)))
  (setq viewer-name (concat "viewer-" pdf-file-name))
  (if (eq (get-process viewer-name) nil)
      (start-process viewer-name lvb "/Applications/Skim.app/Contents/MacOS/Skim" pdf-file-name )));; open the pdf
(defun save-compile-latex-nomal ()
  (interactive)
  (save-compile-latex-base "/Users/kojimayasuhiro/usr/bin/mkpdf"))
(defun save-compile-latex-bib ()
  (interactive)
  (save-compile-latex-base "/Users/kojimayasuhiro/usr/bin/mkpdfbib"))  
(add-hook 'latex-mode-hook
	  '(lambda ()
	     ;;anything-sourcesにshell履歴を追加
					;(setq anything-tmp (cons (car anything-sources) (cons 'anything-c-source-shell-history (cdr anything-sources))))
					;(set (make-local-variable 'anything-sources) nil)
					;(set (make-local-variable 'anything-sources) anything-tmp)
	     ;;ESC keyがそのままきく
					;(define-key term-raw-map (kbd "ESC") 'term-send-raw)
	     ;;コマンドを検索
	     (flyspell-mode 1)))
(bind-keys
 :map latex-mode-map
 ("C-w" . save-compile-latex-nomal)
 ("C-c w" . save-compile-latex-bib)
 ("C-j" . auto-complete-mode))
