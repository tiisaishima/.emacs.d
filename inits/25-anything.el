(require 'anything)
(require 'anything-config)
(require 'anything-c-shell-history)
(setq anything-sources
      '(anything-c-source-buffers+
	anything-c-source-recentf
	anything-c-source-files-in-current-dir
	;anything-c-source-shell-history
	anything-c-source-emacs-commands
	anything-c-source-complex-command-history
	))
(require 'recentf)
(setq recentf-max-saved-items 1000)
(recentf-mode 1)
;; (@* "keymaps")
;; (find-ekeymapdescr anything-map)
;;(define-key anything-map "\C-k" 'anything-select-action-in-minibuffer)
(define-key anything-map "\C-k" (lambda () (interactive) (delete-minibuffer-contents)))
(setq anything-map-C-j-binding 'anything-select-3rd-action)
(define-key anything-map "\C-j" anything-map-C-j-binding)
(define-key anything-map "\C-e" 'anything-select-2nd-action-or-end-of-line)
(define-key anything-map "\C-s" 'anything-isearch)
(define-key anything-map "\C-v" 'anything-next-page)
(define-key anything-map "\M-v" 'anything-previous-page)
(define-key anything-map "\C-z" 'anything-execute-persistent-action)
(define-key anything-map "\C-i" 'anything-select-action)
(define-key anything-map "\M-n" (kbd "C-u 5 <down>"))
