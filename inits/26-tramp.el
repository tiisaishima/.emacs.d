;(add-to-load-path "/webkit/")
;(require 'epc)				;
;(load "webkit.el")
(require 'tramp)
(setq-default tramp-default-method "ssh")
(setq tramp-chunksize 500)
(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
(custom-set-variables '(tramp-verbose 8))
(setq tramp-shell-prompt-pattern "^.*[#$%>].*")
(setq tramp-debug-buffer t)
(setenv "TMPDIR" "/tmp")
