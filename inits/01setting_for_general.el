					;エラー時にフラッシュさせない
(setq visible-bell t)
(setq ring-bell-function 'ignore)
(setq eval-expression-print-length nil)
;;sound
(defvar play-sound-external-command '("mplayer" "-really-quiet"))
(defun play-sound (sound)
  (apply 'call-process
         `(,(car play-sound-external-command) nil nil nil
           ,@(cdr play-sound-external-command)
           ,(file-truename (plist-get (cdr sound) :file)))))
;; mac specific settings
(when (eq system-type 'darwin) ;; mac specific settings
  (setq mac-function-modifier 'control)  
  (setq mac-option-modifier 'control)
  (setq mac-command-modifier 'meta)
  (global-set-key [kp-delete] 'delete-char) ;; sets fn-delete to be right-delete
  )
;;最後のマークに移動
(defun move-to-mark ()
  (interactive)
  (let ((pos (point)))
    (goto-char (mark))
    (push-mark pos)))
;;今いるバッファーを消す
(defun kill-current-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))
;; load environment variables
;; 追記 GEMに関する環境変数を設定すると rbenv経由で rubyがうまく使えなかったので削除
(let ((envs '("PATH" "VIRTUAL_ENV" "GOROOT" "GOPATH")))
  (exec-path-from-shell-copy-envs envs))
(setq search-default-regexp-mode nil) ;; emacs25.1
